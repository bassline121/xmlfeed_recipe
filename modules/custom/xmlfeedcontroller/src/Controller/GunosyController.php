<?php

/**
 * Gunosy test RSS
 * See this in action at /export_xml/articles.xml
 */

  namespace Drupal\export_xml\Controller;

  use Drupal\Core\Controller\ControllerBase;
  use Drupal\image\Entity\ImageStyle;
  use Drupal\media\Entity\Media;
  use Drupal\node\Entity\Node;
  use Symfony\Component\HttpFoundation\Response;

  class GunosyController extends ControllerBase {

  public function export_xml() {
    $host = (empty($_SERVER["HTTPS"]) ? "http://" : "https://") . $_SERVER["HTTP_HOST"];
    
      //ロゴ画像を差し替えている場合
      //    $filename = 'sites/recipe/files/logo.png'; // マルチサイトの場合
      $filename = 'sites/default/files/logo.png'; // 単体サイトの場合
      if (!file_exists($filename)) {
          $filename = '/core/profiles/demo_umami/themes/umami/logo.svg';
      }
    $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $response = new Response();
    $response->headers->set('Content-Type', 'text/xml');
    // Setup general nid query.
    $query = \Drupal::entityQuery('node');
    $query->condition('type', array('recipe'), 'IN');
    $query->condition('status',1);
    $query->condition('langcode', $language); //rangeの件数より少ない場合でも出力
    $query->sort('changed', 'DESC');
    $query->sort('created', 'DESC');
    //    $query->range(0, 20); //件数指定場合はコメントアウト取る
    $nids = array_values($query->execute());

    foreach ($nids as $key => $value) {
      $result[$key] = self::xmlOutput($value);
      //最終記事更新時間取得
      $lastdate_array = explode('>',$result[0]);
        $language_arr = strstr($lastdate_array[13],'<',true);//
    }

    // Prepend XML wrapper.
    array_unshift($result, '<?xml version="1.0" encoding="utf-8"?>' . PHP_EOL,
      '<rss version="2.0" xmlns:gnf="http://assets.gunosy.com/media/gnf" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:media="http://search.yahoo.com/mrss/">' . PHP_EOL,
      '<channel>' . PHP_EOL,
      '<title>' . \Drupal::config('system.site')->get('name') . '</title>' . PHP_EOL,
      '<link>' . (empty($_SERVER["HTTPS"]) ? "http://" : "https://") . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"] . '</link>' . PHP_EOL,
      '<description>スキルやノウハウを雑誌限定記事、レシピ、今すぐ手に入れるべき理由が満載</description>' . PHP_EOL,
      '<ttl>10</ttl>' . PHP_EOL,
        '<image>' . PHP_EOL,
        '<url>' . $host . '/' . $filename . '</url>' . PHP_EOL,
        '<title>' . \Drupal::config('system.site')->get('name') . '</title>' . PHP_EOL,
        '<link>' . (empty($_SERVER["HTTPS"]) ? "http://" : "https://") . $_SERVER["HTTP_HOST"] . '</link>' . PHP_EOL,
        '</image>' . PHP_EOL,
      '<language>' . $language . '</language>' . PHP_EOL,
      '<copyright>Terms &amp; Conditions</copyright>' . PHP_EOL,
      '<lastBuildDate>' . $language_arr . '</lastBuildDate>' . PHP_EOL);

    // Append XML closure.
    $result[] = '</channel>' . PHP_EOL . '</rss>';
    $response->setContent(implode('', $result));

    return $response;
  }

  public function xmlOutput($input) {
    $language = \Drupal::languageManager()->getCurrentLanguage()->getId();

    //Get the article node
    $node = Node::load($input);

    $recipe_instruction = $node->getTranslation($language)->get('field_recipe_instruction')->getValue();
    $recipe_instruction_label = $node->getTranslation($language)->field_recipe_instruction->getFieldDefinition()->getLabel();

    $ingredients = $node->getTranslation($language)->get('field_ingredients')->getValue();
    $ingredients_label = $node->getTranslation($language)->field_ingredients->getFieldDefinition()->getLabel();

    $summary = $node->getTranslation($language)->field_summary->value;

    $difficulty = $node->getTranslation($language)->field_difficulty->value;

    $difficulty_label = $node->getTranslation($language)->field_difficulty->getFieldDefinition()->getLabel();

    $number_of_servings = $node->field_number_of_servings->value;
    $number_of_servings_label = $node->getTranslation($language)->field_number_of_servings->getFieldDefinition()->getLabel();

    $cooking_time = $node->field_cooking_time->value;
    $cooking_time_label = $node->getTranslation($language)->field_cooking_time->getFieldDefinition()->getLabel();

    $preparation_time = $node->field_preparation_time->value;
    $preparation_time_label = $node->getTranslation($language)->field_preparation_time->getFieldDefinition()->getLabel();

    $recipe_edit = '<table>' . PHP_EOL;
    $recipe_edit .= '<tr><th>' . $preparation_time_label .'</th><td>' . $preparation_time .'分</td></tr>' . PHP_EOL;
    $recipe_edit .= '<tr><th>' . $cooking_time_label .'</th><td>' . $cooking_time .'分</td></tr>' . PHP_EOL;
    $recipe_edit .= '<tr><th>' . $number_of_servings_label .'</th><td>' . $number_of_servings .'</td></tr>' . PHP_EOL;
    $recipe_edit .= '<tr><th>' . $difficulty_label .'</th><td>' . $difficulty .'</td></tr>' . PHP_EOL;
    $recipe_edit .= '</table>' . PHP_EOL;

    $ingredientsOutput = '<ul>' . PHP_EOL;
    $ingredientsOutput .= '<h3>' . $recipe_instruction_label .'</h3>' . PHP_EOL;
    foreach ($ingredients as $key => $value) {
      $ingredientsOutput .= '<li>' . $value['value'] . '</li>' . PHP_EOL;
      if($key === array_key_last($ingredients)){
        $ingredientsOutput .= '</ul>' . PHP_EOL;
      }
    }

    //Get article node ID
    $nid = $node->id();

    //Get article title
    $title = $node->getTranslation($language)->getTitle();

    $url_alias = \Drupal::service('path_alias.manager')->getAliasByPath('/node/' . $nid);
    $pubDate = date('r', $node->get('created')->value);
    $changed = date('r', $node->get('changed')->value);

    //Get article image
    $media_image = $node->getTranslation($language)->get('field_media_image')->target_id; // ファイルID
    $media_entity_load = Media::load($media_image); // Loading media entity.
    $imageURL = ImageStyle::load('large')->buildUrl($media_entity_load->field_media_image->entity->getFileUri());
    $image_output = '<enclosure url="' . $imageURL . '" type="image/jpeg" length="0" />' . PHP_EOL;
    $image_output_edit = '<figure><img src="' . $imageURL .'"></figure>' . PHP_EOL;

    //description
    if ($summary) {
      $summary = str_replace("<p>", "", $summary);
      $summary = str_replace("</p>", "", $summary);
      $summary_edit =  '<description><![CDATA[' . htmlspecialchars_decode($summary, ENT_QUOTES) . ']]></description>' . PHP_EOL;
    }

    //content:encoded
    if (isset($media_image)) {
      $body_content_edit = '<content:encoded><![CDATA[' . htmlspecialchars_decode($image_output_edit . $recipe_edit . $ingredientsOutput . '<h3>' . $ingredients_label .'</h3>' . $recipe_instruction[0]['value'], ENT_QUOTES) . ']]></content:encoded>' . PHP_EOL;
    }

    //掲載判定
    if ($node->get('status')->value == '1') {
        $status_edit = '<media:status state="active" />' . PHP_EOL;
      }
      else {
        $status_edit = '<media:status state="deleted " />' . PHP_EOL;
      }

    //カテゴリ
      $news_category_edit = '<gnf:category>gourmet</gnf:category>' . PHP_EOL;

      //関連記事
//    $related_output = '';
//    if($node->getType() == 'news' and $node->get('field_related')->getValue()) {
//      $related = $node->get('field_related')->getValue();
//      foreach ($related as $v) {
//        $related_output .= '<gnf:relatedLink title="' . $v['title'] . '" link="' . $v['uri'] . '" />' . PHP_EOL;
//      }
//    }

    $ad_tag = <<<EOD
<gnf:analytics>
<![CDATA[
 <script> (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){ (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-xxxx-1', 'auto');
    ga('set', 'appName', 'newspass');
    ga('send', 'pageview');
</script> ]]>
</gnf:analytics>
<gnf:analytics_gn>
<![CDATA[ <script> (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){ (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) })(window,document,'script','//www.google-analytics.com/analytics.js','ga'); ga('create', 'UA-xxxx-1', 'auto'); ga('set', 'appName', 'gunosy'); ga('send', 'pageview'); </script> ]]>
</gnf:analytics_gn>
EOD;

    //compile our XML markup with values received above
    $compiled = [
      '<item whereValue="' . $nid . '">' . PHP_EOL,
      '<title>' . htmlspecialchars_decode($title, ENT_QUOTES) . '</title>' . PHP_EOL,
      '<link>' . $node->getTranslation($language)->toUrl()->setAbsolute()->toString() . '</link>' . PHP_EOL,
      '<guid>' . $node->getTranslation($language)->toUrl()->setAbsolute()->toString() . '</guid>' . PHP_EOL,
      $news_category_edit,
      $status_edit,
      '<pubDate>' . $pubDate . '</pubDate>' . PHP_EOL,
      '<gnf:modified>' . $changed . '</gnf:modified>' . PHP_EOL,
      $summary_edit,
      $image_output,
      $body_content_edit,
//      $related_output,
      $ad_tag . PHP_EOL,
      '</item>' . PHP_EOL,
    ];

    //return the XML markup
    return implode('', $compiled);
  }
}
