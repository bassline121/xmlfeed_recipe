<?php

namespace Drupal\export_xml\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;
use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;
use Drupal\image\Entity\ImageStyle;

class ArticleController extends ControllerBase {
  /**
   * Function (Public): Returns content to my route.
   * See this in action at /xml/articles.xml
   */
  public function export_xml() {
    $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $response = new Response();
    $response->headers->set('Content-Type', 'text/xml');
    // Setup general nid query.
    $query = \Drupal::entityQuery('node');
    $query->condition('type', 'article');
    $query->condition('status',1);
    $query->condition('langcode', $language);
    $query->sort('created', 'DESC');
    $nids = array_values($query->execute());
    foreach ($nids as $key => $value) {
      $result[$key] = self::xmlOutput($value);
    }

    // Prepend XML wrapper.
    array_unshift($result, '<?xml version="1.0" encoding="utf-8"?>' . PHP_EOL,
        '<entities title="Articles">' . PHP_EOL);
    // Append XML closure.
    $result[] = '</entities>' . PHP_EOL;
    $response->setContent(implode('', $result));
    return $response;
  }

  public function xmlOutput($input) {
    $language = \Drupal::languageManager()->getCurrentLanguage()->getId();

    //Get the article node
    $node = Node::load($input);

    //Get article node ID
    $nid = $node->id();

    //Get article title
    $title = $node->getTranslation($language)->getTitle();

    //Get article body
//    $body = $node->get('body')->value;
    $body = $node->getTranslation($language)->get('body')->value;

    //date
    $pubDate = date('r', $node->get('created')->value);

    //compile our XML markup with values received above
    $compiled = [
      '<article  whereValue="' . $nid . '">' . PHP_EOL,
      '<title>' . $title . '</title>' . PHP_EOL,
      '<body><![CDATA[' . $body . ']]></body>' . PHP_EOL,
      '<pubDate>' . $pubDate . '</pubDate>' . PHP_EOL,
      '<id>' . $nid . '</id>' . PHP_EOL,
      '</article>' . PHP_EOL,
    ];

    //return the XML markup
    return implode('', $compiled);
  }
}
